import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:pendo_sdk/pendo_sdk.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      dynamic arguments = {
        "visitorId": "flutter_visitorId1",
        "accountId": "flutter_accountId"
      };
      PendoFlutterPlugin.initSDK("eyJhbGciOiJSUzI1NiIsImtpZCI6IiIsInR5cCI6IkpXVCJ9.eyJkYXRhY2VudGVyIjoidXMiLCJrZXkiOiJiYTAxNzQ3OGVhZGNmYzRkYjI2ZWE1ZmQ4MzJhZTYwZWZhNDM1ODNhYWUyNzcyODE4ZTk1NTQ2NzFjZWVkOGFiMWMyYzMxYjMxMTYzZDE4ZjEwOWI5YWNhZjA0ZWJmZTBkMzMyM2E5NjhlN2I5N2QwOTM0NzdkMDA4MDQ4MDdhY2RjNWM5M2NlOWQyOTA5ZDRhZmJkNmVkODExNTQ4ZmQyNTE3YmEwYWU1ZTA2MGM1NGI0OTc2NTlkMGU2N2NiZjE3M2E0Y2RkOTlmMmFlMmU5M2NkNzBiMmQwNTkzOGUwOS45YWVmNTAwY2QzNTliMjM2M2M0Y2I3Y2E0NmM4OGQyOS4yYmYxNmVhYTkzMjZiMmFjNjU0MDVmMjVjZjMzYTViNDJlOTllZTU2NzM4ZjNiYzAwYTgxOWI5ODIzMzZiNGUwIn0.DJuL2yXQ0e9hX_qnIh1JLMfa_FLDQpqPZK8qV50yV4vlGWSpEpljoXOeDQnkNtAqGNSfmUrIlu5pz8KcjWBsROOI5p5fpwJgIuCItLzMyz3MOto18oOw3VTrWW-W72Nt2tCOzV0_ey_ubmke35VQgN7Fu1eQb4DKIk5F-O1ijoQ", arguments );
      PendoFlutterPlugin.clearVisitor();
      PendoFlutterPlugin.switchVisitor("visitorId","accountId", arguments, arguments);
      PendoFlutterPlugin.setVisitorData(arguments);
      PendoFlutterPlugin.setAccountData(arguments);
      PendoFlutterPlugin.trackEventWithProperties("event",arguments);

    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Text('Running on: $_platformVersion\n'),
        ),
      ),
    );
  }
}
