import 'dart:async';

import 'package:flutter/services.dart';

class PendoFlutterPlugin {
  static const MethodChannel _channel =
      const MethodChannel('pendo_sdk_channel_name');

  static void initSDK(String appKey, dynamic initParams) async {
    try {
      dynamic arguments = {
        "ApplicationKey": appKey,
        "ApplicationParameters": initParams
      };
      await _channel.invokeMethod('initSDK', arguments);
    } catch (e) {
      throw ('Failed calling initSDK() with appKey: ${appKey}, initParams: ${initParams}, error is: ${e}');
    }
  }

  static void initSDKWithoutVisitor(String appKey, [dynamic appParams]) async {
    try {
      dynamic arguments = {
        "ApplicationKey": appKey,
        "ApplicationParameters": appParams
      };
      await _channel.invokeMethod('initSDKWithoutVisitor', arguments);
    } catch (e) {
      throw ('Failed calling initSDKWithoutVisitor() with appKey: ${appKey}, appParams: ${appParams}, error is: ${e}');
    }
  }

  static void clearVisitor() async {
    try {
      await _channel.invokeMethod('clearVisitor');
    } catch (e) {
      throw ('Failed calling clearVisitor(), error is: ${e}');
    }
  }

  static void switchVisitor(String visitorId, String accountId,
      dynamic visitorData, dynamic accountData) async {
    try {
      dynamic arguments = {
        "VisitorIDKey": visitorId,
        "AccountIDKey": accountId,
        "VisitorDataKey": visitorData,
        "AccountDataKey": accountData
      };
      await _channel.invokeMethod('switchVisitor', arguments);
    } catch (e) {
      throw ('Failed calling switchVisitor() with visitorId: ${visitorId}, accountId: ${accountId}, visitorData: ${visitorData}, accountData: ${accountData}, error is: ${e}');
    }
  }

  static void setVisitorData(dynamic visitorData) async {
    try {
      dynamic arguments = {"VisitorDataKey": visitorData};
      await _channel.invokeMethod('setVisitorData', arguments);
    } catch (e) {
      throw ('Failed calling setVisitorData() with visitorData: ${visitorData}, error is: ${e}');
    }
  }

  static void setAccountData(dynamic accountData) async {
    try {
      dynamic arguments = {"AccountDataKey": accountData};
      await _channel.invokeMethod('setAccountData', arguments);
    } catch (e) {
      throw ('Failed calling setAccountData() with accountData: ${accountData}, error is: ${e}');
    }
  }

  static void track(String event, dynamic properties) async {
    try {
      dynamic arguments = {
        "EventNameKey": event,
        "EventPropertiesKey": properties
      };
      await _channel.invokeMethod('track', arguments);
    } catch (e) {
      throw ('Failed calling track() with event: ${event}, properties: ${properties}, error is: ${e}');
    }
  }

  static void endSession() async {
    try {
      await _channel.invokeMethod('endSession');
    } catch (e) {
      throw ('Failed calling endSession(), error is: ${e}');
    }
  }

  static void setAccountId(String accountId) async {
    try {
      dynamic arguments = {"AccountIDKey": accountId};
      await _channel.invokeMethod('setAccountId', arguments);
    } catch (e) {
      throw ('Failed calling setAccountId() with accountId: ${accountId}, error is: ${e}');
    }
  }

  static void pauseGuides(bool dismissGuides) async {
    try {
      dynamic arguments = {"DismissGuidesKey": dismissGuides};
      await _channel.invokeMethod('pauseGuides', arguments);
    } catch (e) {
      throw ('Failed calling pauseGuides() with dismissGuides: ${dismissGuides}, error is: ${e}');
    }
  }

  static void dismissVisibleGuides() async {
    try {
      await _channel.invokeMethod('dismissVisibleGuides');
    } catch (e) {
      throw ('Failed calling dismissVisibleGuides(), error is: ${e}');
    }
  }

  static void resumeGuides() async {
    try {
      await _channel.invokeMethod('resumeGuides');
    } catch (e) {
      throw ('Failed calling resumeGuides(), error is: ${e}');
    }
  }

  static Future<String?> getVisitorId() async {
    try {
      return await _channel.invokeMethod('getVisitorId');
    } catch (e) {
      throw ('Failed calling getVisitorId(), error is: ${e}');
    }
  }

  static Future<String?> getAccountId() async {
    try {
      return await _channel.invokeMethod<String>('getAccountId');
    } catch (e) {
      throw ('Failed calling getAccountId(), error is: ${e}');
    }
  }

  static Future<String?> getDeviceId() async {
    try {
      return await _channel.invokeMethod<String>('getDeviceId');
    } catch (e) {
      throw ('Failed calling getDeviceId(), error is: ${e}');
    }
  }
}
