#import "PendoFlutterPlugin.h"
#import <Pendo/Pendo.h>


static NSString *const kPNDPendoChannelName = @"pendo_sdk_channel_name";
static NSString *const kPNDPendoPluginName = @"pendo_sdk";

@implementation PendoFlutterPlugin

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    FlutterMethodChannel* channel = [FlutterMethodChannel methodChannelWithName:kPNDPendoChannelName binaryMessenger:[registrar messenger]];
    PendoFlutterPlugin* instance = [[PendoFlutterPlugin alloc] init];
    [registrar addMethodCallDelegate:instance channel:channel];
}

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
    NSObject<FlutterPluginRegistrar> *registrar = [registry registrarForPlugin:kPNDPendoPluginName];
    [self registerWithRegistrar:registrar];
}

- (void)handleMethodCall:(FlutterMethodCall *)call result:(FlutterResult)result {
    if([@"initSDK" isEqualToString:call.method]) {
        if (nil != call.arguments && [call.arguments isKindOfClass:[NSDictionary class]]) {
            NSDictionary *arguments = (NSDictionary *)call.arguments;
            NSString *appKey = arguments[@"ApplicationKey"];
            NSDictionary *initParams = arguments[@"ApplicationParameters"];
            [self initSDK:appKey initParams:initParams];
        }
    } else if([@"initSDKWithoutVisitor" isEqualToString:call.method]) {
        if (nil != call.arguments && [call.arguments isKindOfClass:[NSDictionary class]]) {
            NSDictionary *arguments = (NSDictionary *)call.arguments;
            NSString *appKey = arguments[@"ApplicationKey"];
            NSDictionary *sdkArgs = arguments[@"ApplicationParameters"];

            if (![sdkArgs isKindOfClass:[NSNull class]]) {
                NSString *environmentName = sdkArgs[@"kEnvironmentName"];
                BOOL disableAnalytics = [sdkArgs[@"kDisableAnalytics"] boolValue];
                NSDictionary *configs = sdkArgs[@"kConfigs"];

                PendoOptions *options = [[PendoOptions alloc] init];
                options.environmentName = environmentName;
                options.disableAnalytics = disableAnalytics;
                options.configs = configs;

                [self initSDKWithoutVisitor:appKey withOptions:options];
            } else {
                [self initSDKWithoutVisitor:appKey];
            }
        }
    } else if([@"clearVisitor" isEqualToString:call.method]) {
        [self clearVisitor];
    } else if([@"switchVisitor" isEqualToString:call.method]) {
        if (nil != call.arguments && [call.arguments isKindOfClass:[NSDictionary class]]) {
            NSDictionary *arguments = (NSDictionary *)call.arguments;
            NSString *visitorId = arguments[@"VisitorIDKey"];
            NSString *accountId = arguments[@"AccountIDKey"];
            NSDictionary *visitorData = arguments[@"VisitorDataKey"];
            NSDictionary *accountData = arguments[@"AccountDataKey"];
            [self switchVisitor:visitorId accountId:accountId visitorData:visitorData accountData:accountData];
        }
    } else if([@"setVisitorData" isEqualToString:call.method]) {
        if (nil != call.arguments && [call.arguments isKindOfClass:[NSDictionary class]]) {
            NSDictionary *arguments = (NSDictionary *)call.arguments;
            NSDictionary *visitorData = arguments[@"VisitorDataKey"];
            [self setVisitorData:visitorData];
        }
    } else if([@"setAccountData" isEqualToString:call.method]) {
        if (nil != call.arguments && [call.arguments isKindOfClass:[NSDictionary class]]) {
            NSDictionary *arguments = (NSDictionary *)call.arguments;
            NSDictionary *accountData = arguments[@"AccountDataKey"];
            [self setAccountData:accountData];
        }
    } else if([@"track" isEqualToString:call.method]) {
        if (nil != call.arguments && [call.arguments isKindOfClass:[NSDictionary class]]) {
            NSDictionary *arguments = (NSDictionary *)call.arguments;
            NSString *eventName = arguments[@"EventNameKey"];
            NSDictionary *properties = arguments[@"EventPropertiesKey"];
            [self track:eventName properties:properties];
        }
    } else if ([@"getVisitorId" isEqualToString:call.method]) {
        result([self visitorId]);
    } else if ([@"getAccountId" isEqualToString:call.method]) {
        result([self accountId]);
    } else if ([@"getDeviceId" isEqualToString:call.method]) {
        result([self deviceId]);
    } else if ([@"setAccountId" isEqualToString:call.method]) {
        NSLog(@"[PendoPlugin] setAccountId: is not implemented for iOS. please use switchVisitor API.");
    } else if ([@"endSession" isEqualToString:call.method]) {
        [self endSession];
    } else if ([@"pauseGuides" isEqualToString:call.method]) {
        if (nil != call.arguments && [call.arguments isKindOfClass:[NSDictionary class]]) {
            NSDictionary *arguments = (NSDictionary *)call.arguments;
            BOOL shouldDismissGuidesKey = [arguments[@"DismissGuidesKey"] boolValue];
            [self pauseGuides:shouldDismissGuidesKey];
        }
    } else if ([@"resumeGuides" isEqualToString:call.method]) {
        [self resumeGuides];
    } else if ([@"dismissVisibleGuides" isEqualToString:call.method]) {
        [self dismissVisibleGuides];
    } else {
        result(FlutterMethodNotImplemented);
    }
}

#pragma mark - SDK methods
- (void)initSDK:(nonnull NSString *)appKey initParams:(nullable NSDictionary *)initParams {
    PendoInitParams *sdkInitParams = [[PendoInitParams alloc] init];

    if (initParams != nil) {
        if ([[initParams allKeys] containsObject:@"visitorId"]) {
            NSString *visitorId = initParams[@"visitorId"];
            [sdkInitParams setVisitorId:visitorId];
        }
        
        if ([[initParams allKeys] containsObject:@"accountId"]) {
            NSString *accountId = initParams[@"accountId"];
            [sdkInitParams setAccountId:accountId];
        }
        
        if ([[initParams allKeys] containsObject:@"visitorData"]) {
            NSDictionary *visitorData = initParams[@"visitorData"];
            [sdkInitParams setVisitorData:visitorData];
        }
        
        if ([[initParams allKeys] containsObject:@"accountData"]) {
            NSDictionary *accountData = initParams[@"accountData"];
            [sdkInitParams setAccountData:accountData];
        }

        if ([[initParams allKeys] containsObject:@"environmentName"]) {
            NSString *environmentName = initParams[@"environmentName"];
            PendoOptions *options = [[PendoOptions alloc] init];
            [options setEnvironmentName:environmentName];
            [sdkInitParams setOptions:options];
        }
    }

    [[PendoManager sharedManager] initSDK:appKey initParams:sdkInitParams];
}
/**
 * Must be called <b>after</b> the SDK was initialized.
 * @brief Clears the current visitor.
 */
- (void)clearVisitor {
    [[PendoManager sharedManager] clearVisitor];
}

/**
 * Must be called <b>after</b> the SDK was initialized.
 *
 * @brief Switch to a new visitor.
 *
 * @param visitorId The visitor's ID.
 * @param accountId The account's ID.
 * @param visitorData The visitor's data.
 * @param accountData The account's data.
 */
- (void)switchVisitor:(nullable NSString *)visitorId
            accountId:(nullable NSString *)accountId
          visitorData:(nullable NSDictionary *)visitorData
          accountData:(nullable NSDictionary *)accountData {
    [[PendoManager sharedManager] switchVisitor:visitorId accountId:accountId visitorData:visitorData accountData:accountData];
}

/**
 * Set a visitor data value.
 * This data is used by Pendo Mobile for creating audiences or reporting analytics.
 * For instance you might want to provide data on the visitor's age or if the visitor is logged into a service.
 *
 *  @param visitorData additional visitor data.
 */
- (void)setVisitorData:(nonnull NSDictionary *)visitorData {
    [[PendoManager sharedManager] setVisitorData:visitorData];
}

/**
 * Set account data value for a given data name.
 * This data is used by Pendo Mobile for creating audiences or reporting analytics.
 * For instance you might want to provide data on the account's subscription or if the account is active or not.
 *
 * @param accountData the account data.
 */
- (void)setAccountData:(nonnull NSDictionary *)accountData {
    [[PendoManager sharedManager] setAccountData:accountData];
}

/**
 * When your application needs to send additional events about actions your users perform.
 *
 * @param event The event name describing the user’s action.
 * @param properties dictionary of event properties (optional).
 * @brief Queue a track eventsfor transmission, optionally including properties as the payload of the event.
 */
- (void)track:(nonnull NSString *)event properties:(nullable NSDictionary *)properties {
    [[PendoManager sharedManager] track:event properties:properties];
}

/**
 *  Call this method on the sharedManger with your application key.
 *  Use this API in case the account and visitor data are not yet known, and they will be known at a later point of the app's lifecycle. In order to update the account and visitor data, call the API: [PendoManager switchVisitor:accountId:visitorData:accountData];
 *
 *  @warning This API is NOT an anonymous API. this should only be used if visitor data will be known at a later point.
 *  @param appKey The app key for your account
 */
- (void)initSDKWithoutVisitor:(NSString *_Nonnull)appKey {
    [[PendoManager sharedManager] initSDKWithoutVisitor:appKey];
}

/**
 *  Call this method on the sharedManger with your application key.
 *  Use this API in case the account and visitor data are not yet known, and they will be known at a later point of the app's lifecycle. In order to update the account and visitor data, call the API: [PendoManager switchVisitor:accountId:visitorData:accountData];
 *
 *  @warning This API is NOT an anonymous API. this should only be used if visitor data will be known at a later point.
 *  @param appKey The app key for your account
 *  @param options additional options for internal use only, default nil
 */
- (void)initSDKWithoutVisitor:(NSString *)appKey withOptions:(PendoOptions *_Nullable)options {
    [[PendoManager sharedManager] initSDKWithoutVisitor:appKey withOptions:options];
}

/**
 *  Provide a visitor id to the Pendo Mobile SDK.
 */
- (NSString *)visitorId {
    return [[PendoManager sharedManager] visitorId];
}

/**
 *  Provide a account id to the Pendo Mobile SDK.
 */
- (NSString *)accountId {
    return [[PendoManager sharedManager] accountId];
}

/**
 * Get Pendo SDK unique device id. Used for anonymous visitor id.
 * This id is unique per application.
 */
- (NSString *)deviceId {
    return [[PendoManager sharedManager] getDeviceId];
}

/**
 *  Call this method on the sharedManger to end current session and stop gathering analytics.
 *  @warning Must be called <b>after</b> the SDK was initialized.
 */
- (void)endSession {
    [[PendoManager sharedManager] endSession];
}

/**
 * Call in order to stop showing guides.
 * @param dismissGuides indicates if to dismiss on going guides from screen or not
 */
- (void)pauseGuides:(BOOL)dismissGuides {
    [[PendoManager sharedManager] pauseGuides:dismissGuides];
}

/**
 * Call in order to resume showing guides.
 */
- (void)resumeGuides {
    [[PendoManager sharedManager] resumeGuides];
}

/**
 * Dismiss any visible guides
 */
- (void)dismissVisibleGuides {
    [[PendoManager sharedManager] dismissVisibleGuides];
}

@end
