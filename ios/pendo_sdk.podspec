#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint pendo_sdk.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'pendo_sdk'
  s.version          = '1.1.0'
  s.summary          = 'Pendo SDK Flutter plugin'
  s.description      = <<-DESC
Pendo Flutter plugin
                       DESC
  s.social_media_url    = 'https://www.facebook.com/Pendoio/'
  s.documentation_url   = 'https://help.pendo.io/resources/support-library/unlisted/ios-sdk-integration.html'
  s.homepage            = 'https://www.pendo.io/'
  s.license             = { :file => '../LICENSE' }
  s.author              = { 'Pendo.io' => 'pendo-iOS@pendo.io' }
  s.source              = { :path => '.' }
  s.source_files        = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.platform            = :ios, '9.0'

  # Flutter.framework does not contain a i386 slice. Only x86_64 simulators are supported.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'VALID_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }

  s.dependency 'Flutter'
  s.dependency 'Pendo', '~> 2.9'
end
