package pendo.io.pendo_sdk;

import android.app.Activity;
import android.app.Application;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Iterator;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import sdk.pendo.io.Pendo;

/** PendoFlutterPlugin */
public class PendoFlutterPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {
  final static String pendoChannelName = "pendo_sdk_channel_name";
  private Activity mActivity;

  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;

  public PendoFlutterPlugin() {
    super();
  }
  private PendoFlutterPlugin(Activity activity) {
    super();
    this.mActivity = activity;
  }

  /**  FlutterPlugin **/
  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), pendoChannelName);
    channel.setMethodCallHandler(this);
  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  public static void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), pendoChannelName);
    channel.setMethodCallHandler(new PendoFlutterPlugin(registrar.activity()));
  }


  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  /**  ActivityAware **/
  @Override
  public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
    this.mActivity = binding.getActivity();
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {
    this.mActivity = null;
  }

  @Override
  public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
    this.mActivity = binding.getActivity();
  }

  @Override
  public void onDetachedFromActivity() {
    this.mActivity = null;
  }

  /**  MethodCallHandler **/
  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    switch (call.method) {
      case "initSDK":
        if (call.arguments instanceof HashMap) {
          HashMap arguments  = (HashMap) call.arguments;
          String  appKey     = (String ) arguments.get("ApplicationKey");
          HashMap initParams = (HashMap) arguments.get("ApplicationParameters");
          if (appKey != null) {
            initSDK(appKey, initParams);
          }
        }
        break;

      case "clearVisitor":
        clearVisitor();
        break;

      case "switchVisitor":
        if (call.arguments instanceof HashMap) {
          HashMap arguments = (HashMap) call.arguments;
          String    visitorId = (String) arguments.get("VisitorIDKey");
          String    accountId = (String) arguments.get("AccountIDKey");
          HashMap visitorData = (HashMap) arguments.get("VisitorDataKey");
          HashMap accountData = (HashMap) arguments.get("AccountDataKey");
          switchVisitor(visitorId,accountId,visitorData,accountData);
        }
        break;

      case "setVisitorData":
        if (call.arguments instanceof HashMap) {
          HashMap arguments = (HashMap) call.arguments;
          HashMap data      = (HashMap) arguments.get("VisitorDataKey");
          setVisitorData(data);
        }
        break;

      case "setAccountData":
        if (call.arguments instanceof HashMap) {
          HashMap arguments = (HashMap) call.arguments;
          HashMap data      = (HashMap) arguments.get("AccountDataKey");
          setAccountData(data);
        }
        break;

      case "initSDKWithoutVisitor":
        if (call.arguments instanceof HashMap) {
          HashMap arguments = (HashMap) call.arguments;
          String appKey = (String) arguments.get("ApplicationKey");
          HashMap appParams = (HashMap) arguments.get("ApplicationParameters");
          initSdkWithoutVisitor(appKey, appParams);
        }
        break;

     
      case "track":
        if (call.arguments instanceof HashMap) {
          HashMap arguments = (HashMap) call.arguments;
          String   eventName = (String) arguments.get("EventNameKey");
          HashMap properties = (HashMap) arguments.get("EventPropertiesKey");
          track(eventName, properties);
        }
        break;

      case "endSession":
        endSession();
        break;

      case "resumeGuides":
        resumeGuides();
        break;

      case "dismissVisibleGuides":
        dismissVisibleGuides();
        break;

      case "setAccountId":
        if (call.arguments instanceof HashMap) {
          HashMap arguments = (HashMap) call.arguments;
          String accountId = (String) arguments.get("AccountIDKey");
          setAccountId(accountId);
        }
        break;
        
      case "pauseGuides":
        if (call.arguments instanceof HashMap) {
          HashMap arguments = (HashMap) call.arguments;
          boolean dismissGuides = (boolean) arguments.get("DismissGuidesKey");
          pauseGuides(dismissGuides);
        }
        break;

        case "getVisitorId":
          result.success(getVisitorId());
        break;

      case "getAccountId":
        result.success(getAccountId());
        break;

      case "getDeviceId":
        result.success(getDeviceId());
        break;

      default:
        result.notImplemented();
        break;
    }
  }

  // public static synchronized void initSDK(Application app,String appKey, PendoInitParams params) {
  private void initSDK(@NonNull String appKey, @Nullable HashMap params) {
    Pendo.PendoInitParams initParams = new Pendo.PendoInitParams();
    initParams.setPendoOptions(new Pendo.PendoOptions.Builder().setIgnoreFirstOnCreate(true).build());
    if (params != null) {
      if (params.containsKey("visitorId")) {
        initParams.setVisitorId((String) params.get("visitorId"));
      }
      if (params.containsKey("accountId")) {
        initParams.setAccountId((String) params.get("accountId"));
      }
      if (params.containsKey("visitorData")) {
        initParams.setVisitorData((HashMap<String, Object>)params.get("visitorData"));
      }
      if (params.containsKey("accountData")) {
        initParams.setAccountData((HashMap<String, Object>)params.get("accountData"));
      }
      if (params.containsKey("environmentName")) {
        String environmentName = (String)params.get("environmentName");
        if (environmentName != "") {
          initParams.setPendoOptions(new Pendo.PendoOptions.Builder().setEnvironmentDebugOnly(environmentName).build());
        }
      }
    }
    Pendo.initSDK(this.mActivity, appKey, initParams);   // call initSDK with initParams as a third parameter (this can be `null`).
  }

  private void clearVisitor() {
    Pendo.clearVisitor();
  }

  private void switchVisitor(String visitorId, String accountId, @Nullable HashMap visitorData, @Nullable HashMap accountData) {
    Pendo.switchVisitor(visitorId, accountId, visitorData, accountData);
  }

  private void setVisitorData(@Nullable HashMap visitorData) {
    Pendo.setVisitorData(visitorData);
  }

  private void setAccountData(@Nullable HashMap accountData) {
    Pendo.setAccountData(accountData);
  }

  private void track(@NonNull String event, @Nullable HashMap properties) {
    if (isNullOrWhiteSpace(event)) {
      throw new IllegalArgumentException("Event name is required");
    }
    Pendo.track(event, properties);
  }

  private void initSdkWithoutVisitor(@NonNull String appKey, @Nullable HashMap appParams) {
    Pendo.PendoOptions.Builder pendoOptions =  new Pendo.PendoOptions.Builder().setIgnoreFirstOnCreate(true);
    if (appParams.containsKey("environmentName")) {
      String environmentName = (String)appParams.get("environmentName");
      if (!isNullOrWhiteSpace(environmentName)) {
        pendoOptions.setEnvironmentDebugOnly(environmentName);
      }
    }
    Pendo.initSdkWithoutVisitor(this.mActivity, appKey, pendoOptions.build());
  }

  private void endSession() {
    Pendo.endSession();
  }

  private void resumeGuides() {
    Pendo.resumeGuides();
  }

  private void dismissVisibleGuides() {
    Pendo.dismissVisibleGuides();
  }

  private void setAccountId(String accountId) {
    Pendo.setAccountId(accountId);
  }

  private void pauseGuides(boolean dismissGuides) {
    Pendo.pauseGuides(dismissGuides);
  }

  private String getVisitorId() {
    return Pendo.getVisitorId();
  }

  private String getAccountId() {
    return Pendo.getAccountId();
  }

  private String getDeviceId() {
    return Pendo.getDeviceId();
  }

  private static boolean isNullOrWhiteSpace(String value) {
    return value == null || value.trim().isEmpty();
  }
}
